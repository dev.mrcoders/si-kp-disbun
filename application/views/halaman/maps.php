<!DOCTYPE html>
<html>
  <head>
    <title>Leaflet CDN</title>
    <style>
      html, body, #map{
        height: 100%;
        margin: 0px;
        padding: 0px
      }
			.leaflet-control-attribution { display:none!important}
    </style>
	
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.3/dist/leaflet.css" integrity="sha256-kLaT2GOSpHechhsozzB+flnD+zUyjE2LlfWPgU04xyI=" crossorigin="" />
<script src="https://unpkg.com/leaflet@1.9.3/dist/leaflet.js" integrity="sha256-WBkoXOwTeyKclOHuWtc+i2uENFpDZ9YPdf5Hf+D7ewM=" crossorigin=""></script>

</head>
<body>
	<div id="map"></div>
	<script>
		var meranti =[1.00176, 102.71264];
		var center = [0.374802398949384, 101.0933010941514];
		var map = L.map('map').setView(center, 10);// load DOM kita yang id nya map dan tampilkan peta
		
		
		
		// BASE MAP (Peta Dasar, biasanya pake Radio
		
		//var stamen_watercolor_ssl = L.tileLayer('https://stamen-tiles.a.ssl.fastly.net/watercolor/{z}/{x}/{y}.jpg');
		var stamen_watercolor = L.tileLayer('http://{s}.tile.stamen.com/watercolor/{z}/{x}/{y}.jpg');
		var stamen_toner = L.tileLayer('http://{s}.tile.stamen.com/toner/{z}/{x}/{y}.png');
		
		//perhatikan bahwa cesium blackmarble itu TMS, jadi koordinat y nya di inverse!
		var cesium_blackmarble =  L.tileLayer('http://cesiumjs.org/blackmarble/{z}/{x}/{-y}.png',{maxZoom:8});
		
		// ini hanya demo kalau tidak dibalik koordinat y nya, maka akan terbalik
		var cesium_blackmarble_TMS =  L.tileLayer('http://cesiumjs.org/blackmarble/{z}/{x}/{y}.png',{maxZoom:8});
		
		
		var tileLayer_ESRI_World_Imagery = L.tileLayer('http://server.arcgisonline.com/ArcGIS/' 
           + 'rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}'
					 ,{ attribution: 'ESRI'
					 });

		//DEFAULT
		cesium_blackmarble.addTo(map);
		
/*
update: 2017-11-12
add Google Tile Server
*/


googleStreets = L.tileLayer('http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}',{
    maxZoom: 20,
    subdomains:['mt0','mt1','mt2','mt3']
});

googleHybrid = L.tileLayer('http://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}',{
    maxZoom: 20,
    subdomains:['mt0','mt1','mt2','mt3']
});

googleSat = L.tileLayer('http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}',{
    maxZoom: 20,
    subdomains:['mt0','mt1','mt2','mt3']
});

googleTerrain = L.tileLayer('http://{s}.google.com/vt/lyrs=p&x={x}&y={y}&z={z}',{
    maxZoom: 20,
    subdomains:['mt0','mt1','mt2','mt3']
});
		
		
		var baseMaps = {
				"Stamen Watercolor": stamen_watercolor,
				"Stamen Toner": stamen_toner,
				"cesium Black Marble": cesium_blackmarble,
				"cesium Black Marble (DEMO TERBALIK)": cesium_blackmarble_TMS,
				"Google Streets":googleStreets,
				"Google Hybrid":googleHybrid,
				"Google Satellite":googleSat,
				"Google Terrain":googleTerrain,
			
				"ESRI World Imagery": tileLayer_ESRI_World_Imagery
		};
		
		
		//////////==========
		// Check-boxed overlay
		//////////==========
		
		// Debug Coordinat
		// see
		// http://leafletjs.com/examples/extending/extending-2-layers.html
		L.GridLayer.DebugCoords = L.GridLayer.extend({
			createTile: function (coords) {
				var tile = document.createElement('div');
				tile.innerHTML = [coords.x, coords.y, coords.z].join(', ');
				tile.style.outline = '1px solid red';
				return tile;
			}
		});

		L.gridLayer.debugCoords = function(opts) {
			return new L.GridLayer.DebugCoords(opts);
		};
		
		L.GridLayer.CanvasCircles = L.GridLayer.extend({
			createTile: function (coords) {
				var tile = document.createElement('canvas');

				var tileSize = this.getTileSize();
				tile.setAttribute('width', tileSize.x);
				tile.setAttribute('height', tileSize.y);

				var ctx = tile.getContext('2d');

				// Draw whatever is needed in the canvas context
				// For example, circles which get bigger as we zoom in
				ctx.beginPath();
				ctx.arc(tileSize.x/2, tileSize.x/2, 4 + coords.z*4, 0, 2*Math.PI, false);
				ctx.fill();

				return tile;
			}
		});

		var canvasCircles = new L.GridLayer.CanvasCircles();
		
		
		var checkboxedLayers = {
			"debug":L.gridLayer.debugCoords()
			,"lingkaran":canvasCircles 
		};
		
L.control.layers( baseMaps, checkboxedLayers).addTo(map);		



var popup = L.popup();

function onMapClick(e) {
    popup
        .setLatLng(e.latlng)
        .setContent("You clicked the map at " + e.latlng.toString()+ " zoom="+map.getZoom()
			+"\n Center: " + JSON.stringify(map.getCenter())
			+"\n Bounds: " + JSON.stringify(map.getBounds())
		).openOn(map);
}

map.on('click', onMapClick);
		
	</script>
</body>
</html>
