<!DOCTYPE html>
<html dir="ltr">
<?php $url = base_url().'assets/themav2/' ?>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <!-- Favicon icon -->
  <link rel="icon" type="image/png" sizes="16x16" href="<?=$url?>assets/images/favicon.png">
  <title>404 Error</title>
  <!-- Custom CSS -->
  <link href="<?=$url?>dist/css/style.min.css" rel="stylesheet">

</head>

<body>
  <div class="main-wrapper">
    <div class="preloader">
      <div class="lds-ripple">
        <div class="lds-pos"></div>
        <div class="lds-pos"></div>
      </div>
    </div>
    <div class="error-box">
      <div class="error-body text-center">
        <h1 class="error-title text-danger">404</h1>
        <h3 class="text-uppercase error-subtitle">PAGE NOT FOUND !</h3>
        <p class="text-muted m-t-30 m-b-30">YOU SEEM TO BE TRYING TO FIND HIS WAY HOME</p>
        <a href="index.html" class="btn btn-danger btn-rounded waves-effect waves-light m-b-40">Back to home</a> 
      </div>
    </div>

  </div>

  <script src="<?=$url?>assets/libs/jquery/dist/jquery.min.js"></script>

  <script src="<?=$url?>assets/libs/popper.js/dist/umd/popper.min.js"></script>
  <script src="<?=$url?>assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>

  <script>
    $('[data-toggle="tooltip"]').tooltip();
    $(".preloader").fadeOut();
  </script>
</body>

</html>