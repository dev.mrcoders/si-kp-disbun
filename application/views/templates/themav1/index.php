<!DOCTYPE html>
<html lang="en">
<?php $url=base_url().'assets/backend/' ?>
<head>
	<meta charset="utf-8">
	<meta content="width=device-width, initial-scale=1.0" name="viewport">

	<title>LOGIN | DISPAN</title>
	<meta content="" name="description">
	<meta content="" name="keywords">

	<!-- Favicons -->
	<?php include_once 'style.php'; ?>

  <!-- =======================================================
  * Template Name: NiceAdmin - v2.4.1
  * Template URL: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

	<?php $class  = $this->router->fetch_class(); ?>
	<?php if ($class != 'login'): ?>
		<!-- ======= Header ======= -->
		<?php include_once 'header.php'; ?>
		<!-- End Header -->

		<!-- ======= Sidebar ======= -->
		<?php #include_once 'aside.php'; ?>
		<!-- End Sidebar-->

		<!-- ====== Main ===== -->
	<?php endif ?>
	<?php $this->load->view($page); ?>

	<!-- End #main -->

	<?php if ($class != 'login'): ?>
		<!-- ======= Footer ======= -->
		<?php include_once 'footer.php'; ?>
		<!-- End Footer -->
	<?php endif ?>

	

	<!-- End #main -->

	


	

	

	

	<a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

	<!-- Vendor JS Files -->
	<script src="<?=$url?>vendor/apexcharts/apexcharts.min.js"></script>
	<script src="<?=$url?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="<?=$url?>vendor/chart.js/chart.min.js"></script>
	<script src="<?=$url?>vendor/echarts/echarts.min.js"></script>
	<script src="<?=$url?>vendor/quill/quill.min.js"></script>
	<script src="<?=$url?>vendor/simple-datatables/simple-datatables.js"></script>
	<script src="<?=$url?>vendor/tinymce/tinymce.min.js"></script>
	<script src="<?=$url?>vendor/php-email-form/validate.js"></script>

	<!-- Template Main JS File -->
	<script src="<?=$url?>js/main.js"></script>

</body>

</html>