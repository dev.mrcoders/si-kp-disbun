<?php $mod = $this->access->modules(); ?>
<aside class="left-sidebar">
    <div class="scroll-sidebar">
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-small-cap"><i class="mdi mdi-navigation"></i> <span class="hide-menu">Navigation</span></li>
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark" href="<?= base_url() ?>dashboard" aria-expanded="false"><i class="mdi mdi-home"></i><span class="hide-menu">Dashboard</span></a>
                </li>
                <?php if ($mod == 'dispan') : ?>
                    <li class="sidebar-item">
                        <a class="sidebar-link waves-effect waves-dark" href="<?= base_url() ?>data-produksi" aria-expanded="false"><i class="mdi mdi-border-none"></i><span class="hide-menu">Data Produksi</span></a>
                    </li>
                    <li class="sidebar-item">
                        <a class="sidebar-link waves-effect waves-dark" href="<?= base_url() ?>data-komoditi" aria-expanded="false"><i class="mdi mdi-border-none"></i><span class="hide-menu">Data Komoditi <?= $mod ?></span></a>
                    </li>
                    <li class="sidebar-item">
                        <a class="sidebar-link waves-effect waves-dark" href="<?= base_url() ?>data-produksi/lihatdata/yearprod" aria-expanded="false"><i class="mdi mdi-border-none"></i><span class="hide-menu">Laporan Tahunan <?= $mod ?></span></a>
                    </li>
                <?php endif ?>
                <?php if ($mod == 'diskan') : ?>
                    <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-chart-bar"></i><span class="hide-menu">Data Produksi</span></a>
                        <ul aria-expanded="false" class="collapse  first-level">
                            <li class="sidebar-item"><a href="<?= base_url() ?>produksi-budidaya" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> Budidaya Ikan</span></a></li>
                            <li class="sidebar-item"><a href="<?= base_url() ?>produksi-benih" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> Benih Ikan</span></a></li>
                            <li class="sidebar-item"><a href="<?= base_url() ?>produksi-olahan" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> Olahan Ikan</span></a></li>
                            <li class="sidebar-item"><a href="<?= base_url() ?>produksi-tangkapan" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu">Tangkapan Ikan</span></a></li>
                        </ul>
                    </li>
                    <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-format-list-bulleted-type"></i><span class="hide-menu">Data Jenis</span></a>
                        <ul aria-expanded="false" class="collapse  first-level">
                            <li class="sidebar-item"><a href="<?= base_url() ?>jenis-ikan" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> Jenis Ikan</span></a></li>
                            <li class="sidebar-item"><a href="<?= base_url() ?>jenis-kegiatan" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> Jenis Kegiatan</span></a></li>
                            <li class="sidebar-item"><a href="<?= base_url() ?>jenis-olahan" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> Jenis Produk Olahan</span></a></li>
                            <li class="sidebar-item"><a href="<?= base_url() ?>jenis-alat" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> Jenis Alat Tangkap</span></a></li>
                        </ul>
                    </li>
                <?php endif ?>
                <?php if ($mod == 'disbun') : ?>
                    <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-chart-bar"></i><span class="hide-menu">Data Produksi</span></a>
                        <ul aria-expanded="false" class="collapse  first-level">
                            <li class="sidebar-item"><a href="<?= base_url() ?>produksi-rakyat" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> Perkebunan Rakyat</span></a></li>
                            <li class="sidebar-item"><a href="<?= base_url() ?>produksi-negara" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> Perkebunan Besar Negara</span></a></li>
                            <li class="sidebar-item"><a href="<?= base_url() ?>produksi-swasta" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> Perkebunan Besar Swasta</span></a></li>
                        </ul>
                    </li>
                    <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-format-list-bulleted-type"></i><span class="hide-menu">Data Jenis</span></a>
                        <ul aria-expanded="false" class="collapse  first-level">
                            <li class="sidebar-item"><a href="<?= base_url() ?>jenis-komoditi" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> Jenis Komoditi</span></a></li>
                        </ul>
                    </li>
                <?php endif ?>

            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>