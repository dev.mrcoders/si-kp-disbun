<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$hostname = parse_url("http://" . $_SERVER['HTTP_HOST'] . str_replace(basename($_SERVER['SCRIPT_NAME']), "", $_SERVER['SCRIPT_NAME']));
$subhost = explode('.', $hostname['host']);
$subdomain = $subhost[0];

$route = Route::map();

$route['default_controller'] = 'login';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['login-checking'] = 'login/checklogin';

//Route module dinas pertanian
$route['data-produksi'] = $subdomain . '/data';
$route['data-produksi/tables'] = $subdomain . '/data/datatableproduksi';
$route['data-produksi/save'] = $subdomain . '/data/save';
$route['data-produksi/databyid'] = $subdomain . '/data/databyid';
$route['data-produksi/delete'] = $subdomain . '/data/delete';
$route['data-produksi/kecamatan'] = $subdomain . '/data/selectkecamatan';
$route['data-produksi/kelurahan'] = $subdomain . '/data/selectkelurahan';
$route['data-produksi/lihatdata/yearprod'] = $subdomain . '/data/lihatdata/yearprod';
// $route['data-produksi/yearprod/datatables'] = $subdomain . '/data/datatableyearprod';

$route['data-komoditi'] = $subdomain . '/komoditi';
$route['data-komoditi/tables'] = $subdomain . '/komoditi/tableskomoditi';
$route['data-komoditi/save'] = $subdomain . '/komoditi/save';
$route['data-komoditi/save/(:num)'] = $subdomain . '/komoditi/save/$1';
$route['data-komoditi/databyid'] = $subdomain . '/komoditi/databyid';
$route['data-komoditi/delete'] = $subdomain . '/komoditi/delete';

//Route module dinas perikanan
$route['jenis-ikan'] = $subdomain . '/jenis/index/ikan';
$route['jenis-ikan/tables'] = $subdomain . '/jenis/tablesjenisikan';
$route['jenis-ikan/save'] = $subdomain . '/jenis/save';
$route['jenis-ikan/save/(:num)'] = $subdomain . '/jenis/save/$1';
$route['jenis-ikan/databyid/([a-z]+)'] = $subdomain . '/jenis/databyid/$1';
$route['jenis-ikan/delete/([a-z]+)'] = $subdomain . '/jenis/delete/$1';

$route['jenis-kegiatan'] = $subdomain . '/jenis/index/kegiatan';
$route['jenis-kegiatan/tables'] = $subdomain . '/jenis/tablesjeniskegiatan';
$route['jenis-kegiatan/save'] = $subdomain . '/jenis/save';
$route['jenis-kegiatan/save/(:num)'] = $subdomain . '/jenis/save/$1';
$route['jenis-kegiatan/databyid/([a-z]+)'] = $subdomain . '/jenis/databyid/$1';
$route['jenis-kegiatan/delete/([a-z]+)'] = $subdomain . '/jenis/delete/$1';

$route['jenis-olahan'] = $subdomain . '/jenis/index/olahan';
$route['jenis-olahan/tables'] = $subdomain . '/jenis/tablesjenisolahan';
$route['jenis-olahan/save'] = $subdomain . '/jenis/save';
$route['jenis-olahan/save/(:num)'] = $subdomain . '/jenis/save/$1';
$route['jenis-olahan/databyid/([a-z]+)'] = $subdomain . '/jenis/databyid/$1';
$route['jenis-olahan/delete/([a-z]+)'] = $subdomain . '/jenis/delete/$1';

$route['jenis-alat'] = $subdomain . '/jenis/index/alat';
$route['jenis-alat/tables'] = $subdomain . '/jenis/tablesjenisalat';
$route['jenis-alat/save'] = $subdomain . '/jenis/save';
$route['jenis-alat/save/(:num)'] = $subdomain . '/jenis/save/$1';
$route['jenis-alat/databyid/([a-z]+)'] = $subdomain . '/jenis/databyid/$1';
$route['jenis-alat/delete/([a-z]+)'] = $subdomain . '/jenis/delete/$1';

// $route['jenis-komoditi'] = $subdomain . '/jenis/index/komoditi';
// $route['jenis-komoditi/tables'] = $subdomain . '/jenis/tablesjenisalat';
// $route['jenis-komoditi/save'] = $subdomain . '/jenis/save';
// $route['jenis-komoditi/save/(:num)'] = $subdomain . '/jenis/save/$1';
// $route['jenis-komoditi/databyid/([a-z]+)'] = $subdomain . '/jenis/databyid/$1';
// $route['jenis-komoditi/delete/([a-z]+)'] = $subdomain . '/jenis/delete/$1';

// $route['produksi-budidaya'] = $subdomain.'/produksi/index/budidaya';
// $route['produksi-budidaya/tables'] = $subdomain.'/produksi/datatables';
// $route['produksi-budidaya/save'] = $subdomain.'/produksi/save';
// $route['produksi-budidaya/save/(:num)'] = $subdomain.'/produksi/save/$1';
// $route['produksi-budidaya/databyid'] = $subdomain.'/produksi/databyid';
// $route['produksi-budidaya/delete'] = $subdomain.'/produksi/delete';
// // $route=[
// 	'default_controller'=>'login',
// 	'login-checking'=>'login/checklogin',
// 	'404_override'=>'',
// 	'translate_uri_dashes'=>FALSE
// ];
