<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Errors extends MX_Controller {

	public function index()
	{
		$this->load->view('error_404');
	}

}

/* End of file Error.php */
/* Location: ./application/controllers/Error.php */