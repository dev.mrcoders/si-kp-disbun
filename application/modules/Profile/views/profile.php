<?php $url = base_url() . 'assets/themav2/' ?>
<div class="container-fluid">
    <div class="row justify-content-center">
        <!-- Column -->
        <div class="col-lg-8 col-xlg-9 col-md-7">
            <div class="card p-5">
                <!-- Tabs -->
                <ul class="nav nav-pills custom-pills" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="pills-profile-tab" data-toggle="pill" href="#last-month" role="tab" aria-controls="pills-profile" aria-selected="false">Profile</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-setting-tab" data-toggle="pill" href="#previous-month" role="tab" aria-controls="pills-setting" aria-selected="false">Setting</a>
                    </li>
                </ul>
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="last-month" role="tabpanel" aria-labelledby="pills-profile-tab">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-3 col-xs-6 b-r"> <strong>Nama</strong>
                                    <br>
                                    <p class="text-muted"><span id="nama"></span></p>
                                </div>
                                <div class="col-md-3 col-xs-6 b-r"> <strong>Telepon</strong>
                                    <br>
                                    <p class="text-muted"><span id="telepon"></span></p>
                                </div>
                                <div class="col-md-3 col-xs-6 b-r"> <strong>Email</strong>
                                    <br>
                                    <p class="text-muted"><span id="email"></span></p>
                                </div>
                                <div class="col-md-3 col-xs-6"> <strong>Jabatan</strong>
                                    <br>
                                    <p class="text-muted"><span id="jabatan"></span></p>
                                </div>
                            </div>
                            <hr>
                            <div class="row justify-content-end">
                                <div class="col-sm-3">
                                    <button type="button" class="btn btn-outline-warning" onclick="showHide()">Ubah Data</button>
                                </div>
                            </div>
                            <div id="form">
                                <form class="form-horizontal form-material" action="<?= base_url() ?>profile/save">
                                    <input type="hidden" name="nip" id="nip" value="<?= $this->session->userdata('data')->nip ?>">
                                    <div class="form-group">
                                        <label class="col-md-12">Nama</label>
                                        <div class="col-md-12">
                                            <input type="text" name="nama" placeholder="Johnathan Doe" class="form-control form-control-line nama">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="example-email" class="col-md-12">Email</label>
                                        <div class="col-md-12">
                                            <input type="email" name="email" placeholder="johnathan@admin.com" class="form-control form-control-line email " name="example-email" id="example-email">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12">Telepon</label>
                                        <div class="col-md-12">
                                            <input type="tel" name="telepon" placeholder="+62 8xx xxxx xxxxx" class="form-control form-control-line telepon">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <button type="submit" class="btn btn-success">Update Profile</button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                    <div class="tab-pane fade" id="previous-month" role="tabpanel" aria-labelledby="pills-setting-tab">
                        <div class="card-header">
                            Form Ubah Password
                        </div>
                        <div class="card-body">
                            <form action="" id="ubahpass">
                                <div class="form-group row">
                                    <label for="">Password</label>
                                    <input type="password" class="form-control" name="pass" id="Pass">
                                </div>
                                <div class="form-group row">
                                    <label for="">Ketik Ulang Password</label>
                                    <input type="password" class="form-control" name="confpass" id="confPass">
                                </div>
                                <div class="row">
                                    <span id="check"></span>
                                </div>
                                <div class="row">
                                    <button class="btn btn-danger" type="submit">Ubah</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
    </div>
</div>
<script type="text/javascript">
    var base_url = '<?= base_url() ?>';
    $(function() {
        $('#form').hide();
        let nip = $('#nip').val();
        Data();
        let stat = true;
        $('form#ubahpass').on('keyup', '#confPass', function() {
            if ($(' #confPass').val() != $('#Pass').val()) {
                $('#check').html('Kata Sandi Tidak Cocok').css('color', 'red');
                return stat = false;
            } else if ($(' #confPass').val() == $('#Pass').val()) {
                $('#check').html('Kata Sandi Cocok').css('color', 'green');
                return stat = true;
            }
        });

        $('form#ubahpass').submit(function(e) {
            e.preventDefault();
            let pass = $('#Pass').val();
            let nip = $('#nip').val();
            Swal.fire({
                title: 'Apakah Anda Yakin?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya',
                cancelButtonText: 'Tidak',
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: base_url + 'profile/updatepass',
                        type: "POST",
                        dataType: "json",
                        data: {
                            pass: pass,
                            nip: nip,
                        },
                        success: function(data) {
                            if (data == true) {
                                window.location = base_url + 'login/islogout';
                            }
                        }
                    })
                }
            })
        })

        function Data() {
            $.ajax({
                url: base_url + 'profile/tables',
                type: 'POST',
                dataType: "json",
                data: {
                    nip: nip,
                },
                success: function(data) {
                    $('#nama').html(data.nama)
                    $('#telepon').html(data.phone)
                    $('#email').html(data.email)
                    $('#jabatan').html(data.jabatan)
                    $('.nama').val(data.nama)
                    $('.telepon').val(data.phone)
                    $('.email').val(data.email)
                    $('.jabatan').val(data.jabatan)
                }
            })
        }
    });

    function showHide() {
        if ($('#form').is(':visible')) {
            $('#form').hide();
        } else {
            $('#form').show();
        }
    }
    $('#form').submit(function(event) {
        event.preventDefault();
        let nama = $('.nama').val();
        let telepon = $('.telepon').val();
        let email = $('.email').val();
        let nip = $('#nip').val();
        $.ajax({
            url: base_url + 'profile/save',
            type: "post",
            data: {
                nama: nama,
                email: email,
                telepon: telepon,
                nip: nip,
            },
            success: function(data) {
                setInterval(location.reload(), 5000);
            }
        })
    })
</script>