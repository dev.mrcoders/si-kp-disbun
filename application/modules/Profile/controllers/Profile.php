<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Profile extends MY_Controller
{
	private $template = 'templates/themav2/index';
	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_login', 'auth');
	}
	public function index()
	{
		$data['page'] = 'profile';
		$this->load->view($this->template, $data);
	}

	public function Datatables()
	{
		$nip = $this->input->post('nip');
		$this->db->where('nip', $nip);
		$db = $this->db->get('v_user')->row();

		echo json_encode($db);
	}

	public function save()
	{
		$nip = $this->input->post('nip');
		$data = [
			'nama' => $this->input->post('nama'),
			'telepon' => $this->input->post('telepon'),
			'email' => $this->input->post('email'),
		];
		$result = $this->db->query('UPDATE tb_user SET nama="' . $data['nama'] . '", email="' . $data['email'] . '", telepon="' . $data['telepon'] . '" WHERE nip="' . $nip . '"');
		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
	public function updatepass()
	{
		$nip = $this->input->post('nip');
		$pass = $this->input->post('pass');
		$result = $this->db->query('UPDATE tb_user SET password="' . md5($pass) . '" WHERE nip="' . $nip . '"');
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
}

/* End of file Login.php */
/* Location: ./application/modules/Login/controllers/Login.php */