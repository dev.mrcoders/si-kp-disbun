<main id="main" class="main ms-5 me-5" style="margin-left: 0;margin-right: 0;">

	<div class="pagetitle">
		<h1>HOME</h1>
		<nav>
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="index.html">Home</a></li>
				<li class="breadcrumb-item">Pages</li>
				<li class="breadcrumb-item active">Blank</li>
			</ol>
		</nav>
	</div><!-- End Page Title -->

	<section class="section">
		<div class="row">
			<div class="col-lg-6">

				<div class="card">
					<div class="card-body text-center">
						<h2 class="card-title text-center" style="font-size: 45px;">PADI</h2>
						<p>Data Statistik Produksi Padi</p>
					</div>
					<div class="card-footer text-center">
						<a href="" class="btn btn-primary btn-block btn-sm">Lihat Data</a>
					</div>
				</div>

			</div>

			<div class="col-lg-6">

				<div class="card">
					<div class="card-body text-center">
						<h5 class="card-title" style="font-size: 45px;">HULKULTURA</h5>
						<p>Data Statistik Produksi Sayuran dan buah-buahan</p>
					</div>
					<div class="card-footer text-center">
						<a href="" class="btn btn-primary btn-block btn-sm">Lihat Data</a>
					</div>
				</div>

			</div>
		</div>
	</section>

</main>