<!DOCTYPE html>
<html>
<head>
	<title>Leaflet CDN</title>
	<style>
	html, body, #map{
		height: 100%;
		margin: 0px;
		padding: 0px
	}
	.leaflet-control-attribution { display:none!important}
	</style>
	
	<link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.3/dist/leaflet.css" integrity="sha256-kLaT2GOSpHechhsozzB+flnD+zUyjE2LlfWPgU04xyI=" crossorigin="" />
	<script src="https://unpkg.com/leaflet@1.9.3/dist/leaflet.js" integrity="sha256-WBkoXOwTeyKclOHuWtc+i2uENFpDZ9YPdf5Hf+D7ewM=" crossorigin=""></script>

</head>
<body>
	<div id="map"></div>
	
	<script>
	var meranti =[1.00176, 102.71264];
	var center = [0.339287, 101.025038];
	var map = L.map('map').setView(center, 14);
	L.tileLayer('http://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}',{
		maxZoom: 20,
		subdomains:['mt0','mt1','mt2','mt3']
	}).addTo(map);
	
	</script>
	</body>
	</html>
