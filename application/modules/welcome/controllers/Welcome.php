<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    /**
     * [__construct description]
     *
     * @method __construct
     */
    public function __construct()
    {
        // Load the constructer from MY_Controller
    	parent::__construct();
    }

    /**
     * [index description]
     *
     * @method index
     *
     * @return [type] [description]
     */
    public function index()
    {
        //
    	$this->load->view('maps');
    }


    function http_request($url){
    	
    // persiapkan curl
    	$ch = curl_init(); 

    // set url 
    	curl_setopt($ch, CURLOPT_URL, $url);

    // set user agent    
    	curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');

    // return the transfer as a string 
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

    // $output contains the output string 
    	$output = curl_exec($ch); 

    // tutup curl 
    	curl_close($ch);      
    	return $output;
    }

    public function data($value='')
    {
    	$url_kec = "http://dev.farizdotid.com/api/daerahindonesia/kecamatan?id_kota=1406";
    	$profile = $this->http_request($url_kec);
    	$profile = json_decode($profile, TRUE);

    	foreach ($profile['kecamatan'] as $key => $v) {
    		$kecamatan[]=[
    			'kd_kecamatan'=>$v['id'],
    			'nama_kecamatan'=>$v['nama'],
    		];

    		$url_kel="http://dev.farizdotid.com/api/daerahindonesia/kelurahan?id_kecamatan=".$v['id'];
    		$kelurahan = $this->http_request($url_kel);
    		$kelurahan = json_decode($kelurahan, TRUE);
    		foreach ($kelurahan['kelurahan'] as $key => $kel) {
    			$kelur[]=[
    				'kd_kecamatan'=>$v['id'],
    				'kd_kelurahan'=>$kel['id'],
    				'nama_kelurahan'=>$kel['nama']
    			];
    		}

    	}

    	$this->db->insert_batch('tb_kecamatan', $kecamatan);
    	// $this->db->insert_batch('tb_kelurahan', $kelur);

    // mengembalikan hasil curl
    	$this->output->set_content_type('application/json')->set_output(json_encode(['kelurahan'=>$kelur]));
    }


}
