<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Negara extends MX_Controller
{
	private $template = 'templates/themav2/index';
	protected $module_name = 'disbun';

	public function __construct()
	{
		parent::__construct();
		$this->access->akses($this->module_name);
		// $this->load->model('M_benih', 'benih');
		$this->user = $this->session->userdata('data');
	}
	public function index($page = null)
	{
		$data['page'] = 'produksi/' . $page;
		$this->load->view($this->template, $data);
	}
	public function load($page)
	{
		$this->load->view('produksi/' . $page);
	}
	public function DataTables()
	{
		// $list = $this->pembenihan->get_datatables();
		$data = [];
		$no = 1;
		$post = $this->input->post();
		if ($this->input->post('bulan')) {
			$this->db->where('SUBSTRING(created,6,2)', $this->input->post('bulan'));
		}
		if ($this->input->post('tahun')) {
			$this->db->where('SUBSTRING(created,1,4)', $this->input->post('tahun'));
		}
		if ($this->input->post('kecamatan')) {
			$this->db->where('kd_kecamatan', $this->input->post('kecamatan'));
		}
		$kebun = $this->db->get('tb_data_perkebunan_negara')->result();
		foreach ($kebun as $key) {
			$row = [];
			$namakec = $this->db->get_where('tb_kecamatan', ['kd_kecamatan' => $key->kd_kecamatan])->row();
			$row['created'] = $key->created;
			$row['nama_kecamatan'] = $namakec->nama_kecamatan;
			$row['aksi'] = '<button class="btn btn-outline-warning edit" data-id="' . $key->id_data_perkebunan_negara . '" data-kd="' . $key->kd_kecamatan . '"data-kecamatan="' . $namakec->nama_kecamatan . '" data-ket="edit"><i class="fas fa-edit"></i> Ubah</button><button class="btn btn-outline-danger delete ml-2" data-id="' . $key->id_data_perkebunan_negara . '" data-kecamatan="' . $namakec->nama_kecamatan . '"><i class="fa fa-trash" aria-hidden="true"></i> Hapus</button>';
			$komoditi = $this->db->get('tb_jenis_komoditi')->result();
			$n = 1;
			$arrkomoditi = [];
			foreach ($komoditi as $v) {
				$row2 = [];
				$row2['no'] = $n;
				$row2['nama_komoditi'] = $v->nama_komoditi;
				$datakebun = $this->db->get_where('tb_data_perkebunan_negara_transaksi', ['id_komoditi' => $v->id_komoditi, 'id_data_perkebunan_negara' => $key->id_data_perkebunan_negara])->row();
				$row2['tbm'] = ($datakebun->tbm == '' ? ' ' : number_format($datakebun->tbm));
				$row2['tm'] = ($datakebun->tm == '' ? ' ' : number_format($datakebun->tm));
				$row2['ttr'] = ($datakebun->ttr == '' ? ' ' : number_format($datakebun->ttr));
				$row2['jumlah'] = ($datakebun->jumlah == '' ? ' ' : number_format($datakebun->jumlah));
				$row2['produksi'] = ($datakebun->produksi == '' ? ' ' : number_format($datakebun->produksi));
				$n++;
				$arrkomoditi[] = $row2;
				$row['produksi'] = $arrkomoditi;
			}
			$data[] = $row;
		}
		$output = array(
			"draw" => $this->input->post('draw'),
			"recordsTotal" => count($kebun),
			"recordsFiltered" => count($kebun),
			"data" => $data,
		);

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function DataById()
	{
		$id = $this->input->get('id_data_perkebunan_negara');
		$this->db->select('*');
		$this->db->from('tb_data_perkebunan_negara');
		$this->db->join('tb_kecamatan', 'tb_data_perkebunan_negara.kd_kecamatan = tb_kecamatan.kd_kecamatan');
		$this->db->where('id_data_perkebunan_negara', $id);
		$Data = $this->db->get()->row();
		$this->output->set_content_type('application/json')->set_output(json_encode($Data));
	}

	public function Save($id = null)
	{
		$post = $this->input->post();

		if ($id == null) {
			$data1['kd_kecamatan'] = $post['kec'];
			$data1['created'] = $post['thn'] . '-' . $post['bln'];
			$this->db->insert('tb_data_perkebunan_negara', $data1);
			$id_data = $this->db->insert_id();
		} else {
			$id_data = $id;
		}
		$komoditi = $this->db->get('tb_jenis_komoditi')->result();
		$data = array();
		foreach ($komoditi as $key => $value) {
			$row = array();
			$id_komoditi = $value->id_komoditi;
			if ($post['tbm'][$key] != "" || $post['tm'][$key] != "" || $post['ttr'][$key] != "" || $post['jumlah'][$key] != "" || $post['produksi'][$key] != "") {
				$row['id_data_perkebunan_negara_transaksi'] = $post['id_data_perkebunan_negara_transaksi'][$key];
				$row['id_data_perkebunan_negara'] = $id_data;
				$row['id_komoditi'] = $id_komoditi;
				$row['tbm'] = $post['tbm'][$key];
				$row['tm'] = $post['tm'][$key];
				$row['ttr'] = $post['ttr'][$key];
				$row['jumlah'] = $post['jumlah'][$key];
				$row['produksi'] = $post['produksi'][$key];
				$data[] = $row;
			}
		}
		if ($data == null) {
			$data = 'Tidak ada Transaksi';
		} else {
			if ($id == null) {
				$this->db->insert_batch('tb_data_perkebunan_negara_transaksi', $data);
			} else {
				foreach ($data as $tes) {
					$this->db->where('id_data_perkebunan_negara_transaksi', $tes['id_data_perkebunan_negara_transaksi']);
					$db = $this->db->get('tb_data_perkebunan_negara_transaksi')->row();
					if ($db == null) {
						$this->db->insert('tb_data_perkebunan_negara_transaksi', $tes);
					} else {
						$this->db->set($tes);
						$this->db->where('id_data_perkebunan_negara_transaksi', $tes['id_data_perkebunan_negara_transaksi']);
						$this->db->update('tb_data_perkebunan_negara_transaksi');
					}
				}
				// 	// $this->db->update_batch('tb_data_tangkapan_transaksi', $data, 'id_data_tangkapan');
			}
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}
	public function Delete($data = null)
	{
		$id = $this->input->post('id');
		$table = array('tb_data_perkebunan_negara', 'tb_data_perkebunan_negara_transaksi');
		$this->db->where('id_data_perkebunan_negara', $this->input->post('id'));
		$result = $this->db->delete($table);

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	public function Check()
	{
		$this->db->where('created', $this->input->post('tahun') . '-' . $this->input->post('bulan'));
		$this->db->where('kd_kecamatan', $this->input->post('kec'));
		$IsDataExist = $this->db->get('tb_data_perkebunan_negara');
		if ($IsDataExist->num_rows() > 0) {
			$DataTrx = $this->db->get_where('tb_data_perkebunan_negara_transaksi', ['id_data_perkebunan_negara' => $IsDataExist->row()->id_data_perkebunan_negara]);
			if ($DataTrx->num_rows() > 0) {
				$Response = [
					'status' => 1, // data master dan transaksi ada
					'msg' => 'Data Master dan Data Transaksi Ada, Silahkan Ganti Bulan, Tahun dan Kecamatan',
					'data' => $IsDataExist->row()->id_data_perkebunan_negara,
					'kec' => $this->input->post('kec'),
				];
			} else {
				$Response = [
					'status' => 2, // data master ada tapi data transaksi kosong
					'msg' => 'Data Master Ada, Silahkan Melakukan Penambahan Data',
					'data' => $IsDataExist->row()->id_data_perkebunan_negara,
					'kec' => $this->input->post('kec'),
				];
			}
		} else {
			$Response = [
				'status' => 0, // data master dan transaksi kosong 
				'msg' => 'Data Belum Ada, Silahkan Tambah Data',
				'data' => $IsDataExist->row()->id_data_perkebunan_negara,
				'kec' => $this->input->post('kec'),
			];
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($Response));
	}
	public function DataTablesForm()
	{
		$id_data_perkebunan_negara = $this->input->post('id_data_perkebunan_negara');
		$komoditi = $this->db->get('tb_jenis_komoditi')->result();
		$no = 1;
		foreach ($komoditi as $val) {
			if ($id_data_perkebunan_negara) {
				$this->db->where('id_data_perkebunan_negara', $id_data_perkebunan_negara);
				$attr = '';
			} else {
				$attr = 'readonly';
			}
			$id = $val->id_komoditi;
			$row = array();
			$tb = $this->db->get_where('tb_data_perkebunan_negara_transaksi', ['id_komoditi' => $id])->row();
			$row[] = '<input type="hidden" name="id_data_perkebunan_negara_transaksi[]" value="' . $tb->id_data_perkebunan_negara . '" readonly class="form-control"/>';
			$row[] = $val->nama_komoditi;
			$row[] = '<input name="tbm[]" type="number" min="0" step="1" class="form-control hitung' . $no . '" value="' . $tb->tbm . '"' . $attr . '/>';
			$row[] = '<input name="tm[]" type="number" min="0"  step="1" class="form-control hitung' . $no . '" value="' . $tb->tm . '"' . $attr . '/>';
			$row[] = '<input name="ttr[]" type="number" min="0"  step="1" class="form-control hitung' . $no . '" value="' . $tb->ttr . '"' . $attr . '/>';
			$row[] = '<input name="jumlah[]" type="number" min="0"  step="1" class="form-control jumlah total' . $no . '" value="' . $tb->jumlah . '"readonly/>';
			$row[] = '<input name="produksi[]" type="number" min="0"  step="1" class="form-control " value="' . $tb->produksi . '"' . $attr . '/>';
			$data[] = $row;
			$no++;
		}
		$output = array(
			"draw" => $this->input->post('draw'),
			"recordsTotal" => count($komoditi),
			"recordsFiltered" => count($komoditi),
			"data" => $data,
		);

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}
}

/* End of file Produksi.php */
/* Location: ./application/modules/diskan/controllers/Produksi.php */