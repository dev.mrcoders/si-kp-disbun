<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jenis extends MX_Controller
{
	private $template = 'templates/themav2/index';
	protected $module_name = 'disbun';
	public function __construct()
	{
		parent::__construct();
		$this->access->akses($this->module_name);
	}
	public function index($page)
	{
		$data['page'] = 'jenis/' . $page;
		$this->load->view($this->template, $data);
	}

	public function DataTables()
	{
		$list = $this->db->get('tb_jenis_komoditi')->result();
		$data = array();
		$no = $this->input->post('start');

		foreach ($list as $r) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $r->nama_komoditi;
			$row[] = '<button class="btn waves-effect waves-light btn-outline-dark btn-xs pl-2 pr-2 pt-1 pb-1 edit" data-id="' . $r->id_komoditi . '"><i class="fas fa-pen-square" style="font-size: 1em;"></i> Ubah</button>
			<button class="btn waves-effect waves-light btn-outline-danger btn-danger btn-xs pl-2 pr-2 pt-1 pb-1 delete" data-id="' . $r->id_komoditi . '"><i class="fas fa-trash" style="font-size: 1em;"></i> Hapus</button>';

			$data[] = $row;
		}
		$output = array(
			"draw" => $this->input->post('draw'),
			"recordsTotal" => count($list),
			"recordsFiltered" => count($list),
			"data" => $data,
		);

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function DataById()
	{
		$id = $this->input->get('id');
		$Data = $this->db->query("SELECT * FROM tb_jenis_komoditi WHERE id_komoditi='$id'")->row();
		$this->output->set_content_type('application/json')->set_output(json_encode($Data));
	}

	public function Save($id = null)
	{
		$data = $this->input->post();
		if ($id == null) {
			$no = 0;
			$d = array();
			foreach ($data['jenis'] as $key => $v) {
				$row = array();
				$row['nama_komoditi'] = $data['jenis'][$no];
				$d[] = $row;
				$no++;
			}
			$result = $this->db->insert_batch('tb_jenis_komoditi', $d);
		} else {
			$row = array();
			$row['nama_komoditi'] = $data['jenis'][0];
			$this->db->set($row);
			$this->db->where('id_komoditi', $id);
			$result = $this->db->update('tb_jenis_komoditi');
		}


		$this->output->set_content_type('application/json')->set_output(json_encode($d));
	}

	public function Delete($id)
	{
		$this->db->where('id_komoditi', $id);
		$result = $this->db->delete('tb_jenis_komoditi');
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
}

/* End of file Jenis.php */
/* Location: ./application/modules/diskan/controllers/Jenis.php */