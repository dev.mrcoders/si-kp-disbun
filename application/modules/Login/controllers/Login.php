<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {
	private $template = 'templates/themav2/index';
	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_login','auth');
	}
	public function index()
	{
		$data['page']='loginv2';
		$this->load->view($this->template,$data);
	}

	public function CheckLogin()
	{
		$PostForm=[
			'nip'=>$this->input->post('username'),
			'password'=>md5($this->input->post('password'))
		];
		$IsLogin = $this->auth->IsLogin($PostForm)->row();
		if ($IsLogin) {
			if ($IsLogin->is_user_login == 'Y') {
				$session = array('login' =>TRUE ,'data'=>$IsLogin );
				$this->session->set_userdata( $session );
				redirect(base_url().'dashboard','refresh');

			}else{
				
				redirect(base_url(),'refresh');
			}
			
		}else{
			
			redirect(base_url(),'refresh');
		}
		
		// 
	}

	public function IsLogOut()
	{
		$data_session = array('login'=>"",'data'=>'');
      	$this->session->unset_userdata($data_session);//clear session
      	$this->session->sess_destroy();//tutup session
		redirect(base_url());
	}

}

/* End of file Login.php */
/* Location: ./application/modules/Login/controllers/Login.php */