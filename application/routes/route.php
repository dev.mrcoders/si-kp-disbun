<?php
Route::get('dashboard', 			'dashboard');
Route::get('login/islogout', 			'login/islogout');

Route::any('profile', 'Profile/profile/index', array(), function () {
	Route::get('(:any)', 'Profile/profile/index');
	Route::post('tables', 'Profile/profile/datatables');
	Route::post('save', 'Profile/profile/save');
	Route::post('updatepass', 'Profile/profile/updatepass');
});
// route untuk dinas perkebunan
Route::any('produksi-rakyat', 'disbun/rakyat/index/rakyat', array(), function () {
	Route::get('(:any)', 'disbun/rakyat/index/$1');
	Route::post('tables', 'disbun/rakyat/datatables');
	Route::post('tables-form', 'disbun/rakyat/datatablesform');
	Route::post('check', 'disbun/rakyat/check');
	Route::post('save', 'disbun/rakyat/save');
	Route::post('save/(:num)', 'disbun/rakyat/save/$1');
	Route::get('databyid', 'disbun/rakyat/databyid');
	Route::post('delete', 'disbun/rakyat/delete');
	Route::get('load/(:any)', 'disbun/rakyat/load/$1');
});
Route::any('produksi-swasta', 'disbun/swasta/index/swasta', array(), function () {
	Route::get('(:any)', 'disbun/swasta/index/$1');
	Route::post('tables', 'disbun/swasta/datatables');
	Route::post('uprtables', 'disbun/swasta/datatablesupr');
	Route::post('dataupr', 'disbun/swasta/dataupr');
	Route::post('tables-form', 'disbun/swasta/datatablesform');
	Route::post('check', 'disbun/swasta/check');
	Route::post('save', 'disbun/swasta/save');
	Route::post('saveupr', 'disbun/swasta/saveupr');
	Route::post('save/(:num)', 'disbun/swasta/save/$1');
	Route::get('databyid', 'disbun/swasta/databyid');
	Route::post('delete/(:num)', 'disbun/swasta/delete/$1');
	Route::get('load/(:any)', 'disbun/swasta/load/$1');
});
Route::any('produksi-negara', 'disbun/negara/index/negara', array(), function () {
	Route::get('(:any)', 'disbun/negara/index/$1');
	Route::post('tables', 'disbun/negara/datatables');
	Route::post('uprtables', 'disbun/negara/datatablesupr');
	Route::post('dataupr', 'disbun/negara/dataupr');
	Route::post('tables-form', 'disbun/negara/datatablesform');
	Route::post('check', 'disbun/negara/check');
	Route::post('save', 'disbun/negara/save');
	Route::post('saveupr', 'disbun/negara/saveupr');
	Route::post('save/(:num)', 'disbun/negara/save/$1');
	Route::get('databyid', 'disbun/negara/databyid');
	Route::post('delete/(:num)', 'disbun/negara/delete/$1');
	Route::get('load/(:any)', 'disbun/negara/load/$1');
});
Route::any('jenis-komoditi', 'disbun/jenis/index/komoditi', array(), function () {
	Route::get('(:any)', 'disbun/jenis/index/$1');
	Route::post('tables', 'disbun/jenis/datatables');
	Route::post('save', 'disbun/jenis/save');
	Route::post('save/(:num)', 'disbun/jenis/save/$1');
	Route::get('databyid', 'disbun/jenis/databyid');
	Route::post('delete/(:num)', 'disbun/jenis/delete/$1');
});
Route::prefix('option', function () {
	Route::get('kecamatan', 'disbun/option/kecamatan');
	Route::get('kelurahan', 'disbun/option/kelurahan');
	Route::get('jenisikan', 'disbun/option/jenisikan');
	Route::get('jeniskegiatan', 'disbun/option/jeniskegiatan');
	Route::get('jenisolahan', 'disbun/option/jenisolahan');
	Route::get('jenisalat', 'disbun/option/jenisalat');
	Route::get('ikanbytype/(:any)', 'disbun/option/ikanbytype/$1');
});
